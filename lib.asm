section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_err_string


; Принимает код возврата и завершает текущий процесс
exit: 
	
   	 xor rax, rax,
   	 mov rax, 60
    	syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
  	  xor rax, rax
.loop:
	cmp byte[rdi+rax], 0
	je .end
	inc rax
	jmp .loop

.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, 1
	syscall
    	ret

;Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_err_string:
	call string_length
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, 2
	syscall
    	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rsi, rsp
	mov rax, 1
	mov rdi, 1
	mov rdx, 1	
	syscall
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 10
	call print_char
    	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rsi, rsp
	mov rax, rdi
	push byte 0
	mov rdi, 10
.loop:
	push word 0
	xor rdx, rdx
	add rsp, 1
	div rdi
	add rdx, '0'
	mov byte[rsp], dl
	cmp rax, 0
	je .end
	jmp .loop
.end:
	mov rdi, rsp	
	push rsi
	call print_string
	pop rsi
	mov rsp, rsi	
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	mov r9, rdi
	mov r10, 0x8000000000000000
	and rdi, r10
	cmp rdi, 0
	je .positive
	mov rdi, 0x2d
	call print_char
	mov rdi,r9
	neg rdi
	call print_uint
	ret
.positive:
	mov rdi, r9
	call print_uint
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor r11, r11
	xor r10, r10
	push rdi
	call string_length
	mov rdi, rsi
	mov r10, rax
	call string_length
	cmp rax, r10
	pop rdi
	jne .notEq
	cmp rax, 0
	je .end
.loop:
	mov r10b, byte[rsi+r11]
	cmp byte[rdi + r11], r10b
	jne .notEq
	inc r11
	cmp r11, rax
	jne .loop
	je .end
	
.notEq:
	mov rax, 0
	ret
.end:
	mov rax, 1
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0
    	mov rax, 0
	mov rdi, 0
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
	cmp rax, 0xa
	je .zero
	ret
.zero:
	mov rax, 0
	ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor r10, r10
	;xor r11, r11
	;xor r12, r12
	cmp rsi, 0
	jle .error
.first:
	push rdi
	push rsi
	;mov r12, rsi
	;mov r11, rdi
	call read_char
	;mov rsi, r12
	;mov rdi, r11
	pop rsi
	pop rdi
	cmp rax, 0
	je .zero
	cmp rax, 0x20
	je .first
	cmp rax, 0x9
	je .first
	mov byte[rdi], al
	inc r10
	inc rdi
	cmp r10, rsi
	jl .second
	je .third
	

.second:
	push rdi
	push rsi
	;mov r12, rsi
	;mov r11, rdi
	call read_char
	;mov rsi, r12
	;mov rdi, r11
	pop rsi
	pop rdi
	cmp rax, 31
	jle .end
	mov byte[rdi], al
	inc r10
	inc rdi
	cmp r10, rsi
	jl .second
	je .third
.end:
	mov byte[rdi],0
	sub rdi, r10
	mov rax, rdi
	mov rdx, r10
	ret
	
.third:
	push rdi
	push rsi
	;mov r12, rsi
	;mov r11, rdi
	call read_char
	pop rsi
	pop rdi
	;mov rsi, r12
	;mov rdi, r11
	cmp rax, 32
	jle .end
			
	

.error:
	sub rdi, r10
	mov rax,0	
	ret
.zero:
	mov byte[rdi], al
	mov rax, rdi
	mov rdx, r10
	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
xor rdx, rdx
xor rax, rax
.read:
	mov rsi, [rdi]
	and rsi, 0xff
	cmp rsi, 0
	je .end
	cmp rsi, '0'
	jl .error
	cmp rsi, '9'
	jg .error
.parse:
	inc rdi
	inc rdx
	sub rsi, '0'
	imul rax, 10
	add rax, rsi
	jmp .read

.end:
	ret
		
	
.error:	
	cmp rdx, 0
	jne .end
	mov rdx, 0
	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    	xor rax, rax
	xor rdx, rdx 
	xor r10, r10
.read:
	mov rsi, [rdi]
	and rsi, 0xff
	cmp rsi, 0
	je .end	
	cmp rsi, '0'
	jl .error
	cmp rsi, '9'
	jg .error
.parse:
	inc rdi
	inc rdx
	sub rsi, '0'
	imul rax, 10
	add rax, rsi
	jmp .read
.end:
	cmp r10, 0
	jnz .negNumber
	ret
.error:
	cmp rdx, 0
	je .checkSign
	jmp .end	
.checkSign:
	inc rdi
	cmp rsi, '+'
	je .addPlus
	cmp rsi, '-'
	je .addMinus
	ret
.addPlus:
	inc rdx
	jmp .read
.addMinus:
	inc rdx	
	mov r10, 1
	jmp .read
.negNumber:
	neg rax
	ret
	
	
    

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor r10, r10
	xor rcx, rcx
	call string_length
	inc rax
	cmp rdx, rax
	jl .error
.loop:
	mov cl, byte[rdi + r10]
	mov byte[rsi + r10], cl
	inc r10
	cmp r10, rax
	jl .loop
.end:
	dec rax
	ret
.error:
	mov rax, 0
	ret




