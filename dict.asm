section .text
global find_word
extern string_equals


find_word:
xor rax, rax
.loop:
add rsi, 8
call string_equals
cmp rax, 1
je .found
sub rsi, 8
cmp byte[rsi], 0
je .notFound 
mov rsi, [rsi]
jmp .loop

.found:
sub rsi, 8
mov rax, rsi
ret

.notFound:
mov rax, 0
ret
