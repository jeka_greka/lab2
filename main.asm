section .text
%include "colon.inc"

extern exit
extern print_string
extern read_word
extern find_word
extern print_err_string

global _start

section .data
found_msg: db "Word was found", 10, 0
error_msg: db "Word was not found", 10, 0
welcome_msg: db "Write key word", 10, 0
too_long_msg: db "Your word is too long", 10, 0
;buffer: times 1 db 0

%include "words.inc"

section .text
_start:
	mov rdi, welcome_msg
	call print_string
	push rbp
	sub rsp, 256
	mov rbp, rsp
	mov rsi, 255
	mov rdi, rsp
	call read_word
	cmp rax, 0
	je .word_too_long
	mov rdi, rax
	mov rsi, last
	call find_word
	test rax, rax
	jz .notFound
	mov rsp, rbp
	pop rbp
	mov rdi, found_msg
	call print_string
	mov rdi, 0
	call exit
	

.word_too_long:
	mov rsp, rbp
	pop rbp
	mov rdi, too_long_msg
	call print_err_string
	mov rdi, 1
	call exit

.notFound:
	mov rsp, rbp
	pop rbp
	mov rdi, error_msg
	call print_err_string
	mov rdi, 1
	call exit


