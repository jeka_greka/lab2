ASM=nasm
ASMFLAGS=-f elf64

all:main

main: main.o lib.o dict.o
ld -o main main.o lib.o dict.o

main.o: main.asm colon.inc words.inc
$(ASM) $(ASMFLAGS) main.asm


dict.o: dict.asm
$(ASM) $(ASMFLAGS) dict.asm

lib.o: lib.asm
$(ASM) $(ASMFLAGS) lib.asm

clean:
rm -f main.o dict.o lib.o main